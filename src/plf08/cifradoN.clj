(ns plf08.cifradoN)


(defn cifradoN
  [c]
  (let [xs {
            
            \@ 135
            \` 141
            \! 114
            \A 76
            \a 1
            \Á 77
            \á 2
            \" 115
            \B 78
            \b 3
            \# 116
            \C 79
            \c 4
            \$ 117
            \D 80
            \d 5
            \% 118
            \E 81
            \e 6
            \& 119
            \F 83
            \f 8
            \' 120
            \G 84
            \g 9
            \( 121
            \H 85
            \h 10
            \) 122
            \I 86
            \i 11
            \É 82
            \é 7
            \* 123
            \J 88
            \j 13
            \+ 124
            \K 89
            \k 14
            \, 125
            \L 90
            \l 15
            \- 126
            \M 91
            \m 16
            \Í 87
            \í 12
            \. 127
            \N 92
            \n 17
            \/ 128
            \O 94
            \o 19
            \0 109
            \P 96
            \p 21
            \1 110
            \Q 97
            \q 22
            \Ñ 93
            \ñ 18
            \2 111
            \R 98
            \r 23
            \3 112
            \S 99
            \s 24
            \Ó 95
            \ó 20
            \4 113
            \T 100
            \t 25
            \5 146
            \U 101
            \u 26
            \6 147
            \V 104
            \v 29
            \7 148
            \W 105
            \w 30
            \8 149
            \X 106
            \x 31
            \Y 107
            \y 32
            \: 129
            \Z 108
            \z 33
            \Ú 102
            \ú 27
            \; 130
            \[ 136
            \{ 142
            \< 131
            \\ 137
            \| 143
            \Ü 103
            \ü 28
            \= 132
            \] 138
            \} 144
            \> 133
            \^ 139
            \~ 145
            \? 134
            \_ 140
            
            }
        x (fn [s] (get xs s))]
    (x c)))
