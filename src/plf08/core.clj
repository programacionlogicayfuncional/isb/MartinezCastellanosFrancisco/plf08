( ns plf08.core
  (:gen-class)
  
  (:require
   [clojure.string :as string]
   [plf08.alfabeto :as I]

    [alfabetoDes :as Descifra]
   [plf08.es :as ES]))



(defn cifrado
  [r s]
  (ES/output s (string/join 
                (map I/alfabeto (ES/input r)))))
(cifrado "resources/cifrar.txt" "resources/cifrado.txt")

 
(defn -main
  [a b c]
  ( (= a "CIFRADO")
    (cifrado b c)))





